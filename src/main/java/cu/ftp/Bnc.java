/**
 * Copyright (c) 2006, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */


package cu.ftp;

import java.net.InetSocketAddress;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2006-jun-21
 */
public class Bnc {
    private String hostname;
    private int port;
    private boolean encryptControlConnection;
    private boolean encryptDataConnection;
    private int timeout = 180;
    private boolean up = true;
    private boolean usePassiveMode = true;
    private int sslMode = 1;
    private InetSocketAddress isa;

    public Bnc(String hostname, int port, boolean encryptControlConnection, boolean encryptDataConnection, int timeout, boolean up, boolean usePassiveMode, int sslMode) {
        this.hostname = hostname;
        this.port = port;
        this.encryptControlConnection = encryptControlConnection;
        this.encryptDataConnection = encryptDataConnection;
        this.timeout = timeout;
        this.up = up;
        this.usePassiveMode = usePassiveMode;
        this.sslMode = sslMode;
        this.isa = new InetSocketAddress(hostname, port);
        if (this.isa.isUnresolved()) {
            System.err.println("Found unresolved address " + hostname + ":" + port + " this will cause trouble, since it is impossible to connect to, and thus can't be registered by the ident daemon.");
            System.exit(-1);
        }
        // what does it mean that an ip is unresolved?
    }

    public String getHostname() {
        return hostname;
    }

    public InetSocketAddress getInetSocketAddress() {
        return isa;
    }

    public int getPort() {
        return port;
    }

    public boolean isEncryptControlConnection() {
        return encryptControlConnection;
    }

    public boolean isEncryptDataConnection() {
        return encryptDataConnection;
    }

    public int getTimeout() {
        return timeout;
    }

    public boolean isUsePassiveMode() {
        return usePassiveMode;
    }

    public int getSslMode() {
        return sslMode;
    }

    public boolean isUp() {
        return up;
    }

    public void setUp(boolean up) {
        this.up = up;
    }
}
