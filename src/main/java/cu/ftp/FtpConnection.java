/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftp;

import cu.ssl.DefaultSSLSocketFactory;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.InetSocketAddress;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2008-sep-30 - 17:23:12
 */
public class FtpConnection {
    protected String name;
    protected Socket controlConnection;
    protected Socket dataConnection;
    protected PrintWriter out;
    protected BufferedReader in;
    protected long timeOfLastCommand;
    protected String lastCommand;
    protected String hostname;
    protected int port;
    protected boolean connected = false;
    protected int timeout = 180;
    protected int connectionTimeout = 30;
    protected FtpResponse response;
    protected int sslMode = 1;
    protected boolean connecting = false;
    protected boolean transferring = false;
    protected boolean debug = false;
    protected String pwd = "/";
    protected String password;
    protected boolean encryptedControlConnection;
    protected String username;

    public FtpConnection(String hostname, int port, String username, String password, boolean encryptedControlConnection, int sslMode, String name) {
        this.hostname = hostname;
        this.port = port;
        this.username = username;
        this.password = password;
        this.encryptedControlConnection = encryptedControlConnection;
        this.sslMode = sslMode;
        this.name = name;
    }

    /**
     * This sends the command to the server and collects the response the server sends due to the command.<br>
     * This response is available via #getResponse().<br>
     * To invoke response parsing again (basically only ever needed for 1** commands), use #parseResponse()<br>
     *
     * @param message
     * @throws SendMessageException
     * @throws HostDisconnectedException
     * @see #getResponse()
     * @see #parseResponse() 
     */
    public void send(String message) throws SendMessageException, HostDisconnectedException {
        message = message.trim();
        if (debug) {
            if (message.startsWith("PASS")) {
                System.out.println("[" + name + "] FTP SEND: PASS *****");
            } else {
                System.out.println("[" + name + "] FTP SEND: " + message);
            }
        }
        if (out != null) {
            out.print(message.trim() + "\r\n");
            out.flush();
            timeOfLastCommand = System.currentTimeMillis();
            lastCommand = message;
            if (out.checkError()) { // todo: if we use an OutputStreamWriter instead, it will throw exceptions for us...
                disconnect(true);
                throw new HostDisconnectedException(name + ":" + hostname+":"+port+":"+lastCommand, name);
                // Don't reconnect here, that's up to the calling class.
            } else {
                parseResponse();
            }
        } else {
            throw new SendMessageException("[" + name + "] Send message failed (out was null): " + message);
        }
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    /**
     * Reads the response from the server. always internally called after a send(). Can also be called externally, due to double responses, such as 150+226.
     * @return
     * @throws cu.ftp.HostDisconnectedException
     */
    public FtpResponse parseResponse() throws HostDisconnectedException {
        String message = "";
        int code = 0;
        boolean multiline = false;
        String fullMessage = "";

        try {
            String line = in.readLine();
            if (line == null || "".equals(line)) {
                System.out.println("[" + name + "] Host disconnected while getting response to command: " + this.getLastCommand());
                // note: if this.getLastCommand() is null, then it probably got a socket failure when it was waiting for the first 220 line
                throw new HostDisconnectedException(name + ":" + hostname+":"+port+":"+(lastCommand == null ? "waiting for 220 (first input)" : lastCommand), name);
            } else {
                message = line;
                try {
                    code = Integer.parseInt(line.substring(0,3));
                } catch (NumberFormatException nfe) {
                    System.out.println("[" + name + "] We got an improper response to be able to parse a response code: " + line);
                    // this is a problem when we return from a "stat -l"
                    // which is shouldn't be, since we shoudl get the 213 response first!
                    // HACK MODE: this is a hack to deal with the one-line mode of glftpd 2.0 that doesn't generate proper replies for "stat -l"
                    if (line.startsWith("total ")) {
                        // But we know it's the result from a 'stat -l' so we're faking it
                        // the beginning of data listings always show a "total 98234" with some digits (I have no clue what they mean, and neither does anybody else it seems)
                        code = 213;
                        multiline = true;
                    } else {
                        message = "ERROR: unable to parse response";
                        code = 500;
                        multiline = true; // we do this, so that we can read the rest of the response, just to make sure.
                    }
                }
                // if we find a "-" in the 4:th place, index 3, as in "250-", then it's a multiline response,
                // and we must handle it accordingly
                // start the reading of a multiline response, and no matter what the lines contain, don't stop until you
                // find the same responsecode NOT followed by a "-"
                // actually, we should justy keep reading until there are no more lines.
                // HACK MODE (we need a special way to enter here, so we add a small hack: let us into this loop if we're already in multiline mode)
                if (line.indexOf("-") == 3 || multiline) {
                    multiline = true;
                    // we're found a multiline response
                    fullMessage += line + "\r\n";



// this block is wrong, just read all the rest of the lines. (remove this after cubnc has been updated to reflect the new changes)
// no, we have to do it like this, since we don't get EOF until we close the stream

                    // do this until we find something like "230 ass", i.e. something that does not have the dash in it.
                    // NOTE: this " " (space) is VERY important!
                    while (!line.startsWith(code + " ")) { // _todo: shouldn't we just check is line.charAt(3) == ' ' ?? (no, we shouldn't. if we do, then FEAT from glftpd would break)
                        line = in.readLine();
                        fullMessage += line + "\r\n";
                        // CHECK: we might have problems here with responses that are malformed, containing a different response code at the end, compared to what it started with
                    }

                } else {
                    fullMessage = line;
                }
                if (code == 150) {
                    // if we get reply from a command that might take time, we set the connection to wait infinitly for that command to get back.
                    // i.e. 150 when we do a STOR or RETR.
                    if (dataConnection != null && !dataConnection.isClosed()) {
                        dataConnection.setSoTimeout(0);
                    }
                    controlConnection.setSoTimeout(0);
                    transferring = true;
                } else if (code != 150) { // set a timeout for all commands that aren't transfer commands (150)
                    // we're gotten replies that indicate that a transfer s done, for instance. reset the wait timeout
                    if (dataConnection != null && !dataConnection.isClosed()) {
                        dataConnection.setSoTimeout(timeout * 1000);
                    }
                    controlConnection.setSoTimeout(timeout * 1000);
                    transferring = false;
                }
            }
        } catch (IOException e) {
            //e.printStackTrace();
            if (debug) {
                System.out.println("[" + name + "] FTP READ: " + e.getMessage());
            }
            throw new HostDisconnectedException(name + ":" + hostname+":"+port+":"+lastCommand, name);
        }

        if (debug) {
            if (code == 230) {
                System.out.println("[" + name + "] FTP READ: We're logged in, skipping the banner");
            } else {
                if (message.startsWith("PASS")) {
                    System.out.println("[" + name + "] FTP READ: PASS *****");
                } else {
                    System.out.println("[" + name + "] FTP READ: " + fullMessage);
                }
            }
        }



        response = new FtpResponse(message, code, multiline, fullMessage);
        if (code == 421) { // this means that we got disconnected
	        throw new HostDisconnectedException(name + ":" + hostname + ":" + port + ":" + lastCommand, name);
        }
        return response;
    }

    public boolean connect() {
        connecting = true;
        try {
            if (sslMode == 3) {
                controlConnection = DefaultSSLSocketFactory.getFactory().createSocket(hostname, port);
            } else {
                controlConnection = new Socket();
	            controlConnection.setTcpNoDelay(true);
	            controlConnection.connect(new InetSocketAddress(hostname, port), connectionTimeout * 1000);
            }
            controlConnection.setSoTimeout(timeout * 1000);
            return createStreams();
        } catch (IOException e) {
            // e.printStackTrace();
        }
        connecting = false;
        return false;
    }

    protected boolean createStreams() {
        try {
            controlConnection.setSoTimeout(timeout * 1000);
            in = new BufferedReader(new InputStreamReader(controlConnection.getInputStream()));
            out = new PrintWriter(controlConnection.getOutputStream());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void disconnect(boolean force) {
        try {
            controlConnection.setSoTimeout(1000);
            // do something with linger here? (just do this if there are problems with disconnections)
            if (!force) {
                try {
                    //send("ABOR");
                    send("QUIT");
                } catch (Exception e) {
                    // do nothing
                }
            }
            in.close();
            out.close();
            controlConnection.close();
            if (dataConnection != null) {
                dataConnection.close();
            }
        } catch (IOException ioe) {
            // ioe.printStackTrace();
        }
    }

    public String getLastCommand() {
        return lastCommand;
    }

    public boolean isConnected() {
        return connected;
    }

    public boolean isConnecting() {
        return connecting;
    }

    public long getTimeOfLastCommand() {
        return timeOfLastCommand;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public boolean logon() throws HostDisconnectedException, IOException {
        return logon(true);
    }

    public boolean logon(boolean checkFor220First) throws HostDisconnectedException, IOException {
        connecting = true;
        if (checkFor220First) {
            // if we don't check, response.getCode() will remain the same.
            // this is used when we have send IDNT outside of this function before
            parseResponse();
        }
        if (response.getCode() == 220) {
            // only do this if we're not doing ftps
            if (encryptedControlConnection && sslMode != 3) {
                if (sslMode == 1) {
                    send("AUTH TLS");
                } else if (sslMode == 2) {
                    send("AUTH SSL");
                }
                if (response.getCode() == 234) {
                    controlConnection = DefaultSSLSocketFactory.getFactory().wrapSocket(controlConnection, true);


                    // re-create streams, so that we send data through the ssl-layer.
                    createStreams();
                    // note: the handshake will occur automatically when new data is sent over the ssl connection
                    send("PBSZ 0");
                    if (response.getCode() != 200) {
                        connecting = false;
                        return false;
                    }
                } else {
                    connecting = false;
                    return false;
                }
            }
            send("USER " + username);
            if (response.getCode() == 331) {
                send("PASS " + password);
                //return (response.getCode() == 230);

                if (response.getCode() == 230) {
                    send("PWD");
                    pwd = parsePwd(response.getMessage());
                    //System.out.println("pwd got parsed to: " + pwd);
                    connecting = false;
                    return true;
                } else {
                    connecting = false;
                    return false;
                }

            } else {
                connecting = false;
                return false;
            }
        } else {
            // we weren't even allowed to send the authenticity information.
            connecting = false;
            return false;
        }
    }

    protected String parsePwd(String pwdString) {
        try {
            int dirnameStart = pwdString.indexOf('"') + 1;
            return pwdString.substring(dirnameStart, pwdString.indexOf('"', dirnameStart)).replaceAll("//", "/");
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse PWD from: _" + pwdString + "_", e);
        }
    }

    public boolean isEncryptedControlConnection() {
        return encryptedControlConnection;
    }

    public String getPwd() {
        return pwd;
    }

    public FtpResponse getResponse() {
        return response;
    }

    public boolean cwd(String dir) throws HostDisconnectedException {
        dir = dir.replaceAll("//", "/").trim();
        send("CWD " + dir);
        if (response.getCode() == 250) {
            send("PWD");
            //NOTE: if PWD doesn't work, then what?
            try {
                pwd = parsePwd(response.getMessage());
                return true;
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
                return false;
            }
        } else {
            return false;
        }
        // DONE_todo: this must execute a PWD to make sure we ended up in the proper directory, for the PWD to be correct
        // NOTE: the reported pwd doesn't have anything to do with the wanted pwd.
        // OR, maybe have a setting like "treat symlinks as dirs". ncftp does this, and cubnc. not flashfxp.
    }


}
