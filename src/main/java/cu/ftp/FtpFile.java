/**
 * Copyright (c) 2006, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */


package cu.ftp;

import java.io.File;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2006-jun-21
 */
public class FtpFile {
    private String filename;
    private boolean dir = false;
    private String permissions;
    private String date;
    private String owner;
    private String group;
    private String path;
    private String parentDir;
    private long size;

    public FtpFile(String filename, int size, String permissions, String date, String owner, String group, String parentDir) {
        this.filename = filename;
        this.permissions = permissions;
        this.date = date;
        this.owner = owner;
        this.group = group;
        this.parentDir = parentDir;
        this.size = size;
        this.dir = permissions.charAt(0) == 'd';
    }

    public FtpFile(File file) {
        this.filename = file.getName();
        this.parentDir = file.getParent();
        this.size = file.length();
        this.dir = file.isDirectory();
    }

    public FtpFile(String ftpListString, String parentDir) {
        // -rw-r--r--   1 doffe    SNELHEST    39357 Apr 17 18:56 tno_n280.zip
        // remember what are tabs, and what are not.
        // 1 - tabbed
        // doffe - tabbed
        // SNELHEST - tabbed
        // 39357 - tabbed
        this.parentDir = parentDir;
        String [] parts = ftpListString.split("\\s+", 9);
        this.permissions = parts[0];
        this.owner = parts[2];
        this.group = parts[3];
        this.size = Long.parseLong(parts[4]);
        this.date = parts[5] + " " + parts[6] + " " + parts[7];
        if (permissions.charAt(0) == 'l') {
            // hmm, is this notation correct on all systems? also, what happens if the file being pointed to, or this file, has "->" in its name? 
            this.filename = parts[8].substring(0, parts[8].lastIndexOf("->")).trim();
        } else {
            this.filename = parts[8].trim();
        }
        // if the permission thing starts with an "l", then only get the thing before the "->"
        this.dir = permissions.charAt(0) == 'd';
    }

    public String getName() {
        return filename;
    }

    public long getSize() {
        return size;
    }

    public boolean isDir() {
        return dir;
    }

    public String getParentDir() {
        return parentDir;
    }

    /**
     * Returns the full path of the file in question, from filesystem root all the way to filename.
     * i.e. if it's a file, it'll return something like: /home/joe/myfile.txt
     * and if it's a dir, it'll return something like: /home/joe/myfiles/
     * @return the full and absolute path to the file (or dir)
     */
    public String getAbsolutePath() {
        return (parentDir + "/" + filename).replace("//","/");
    }
    public String toString() {
        return filename;
    }

    // GENERATED (checks filename and size)
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FtpFile)) return false;

        final FtpFile ftpFile = (FtpFile) o;

        if (size != ftpFile.size) return false;
        if (filename != null ? !filename.equals(ftpFile.filename) : ftpFile.filename != null) return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = (filename != null ? filename.hashCode() : 0);
        result = 29 * result + (int) (size ^ (size >>> 32));
        return result;
    }

    public String getExtension() {
        return this.filename.substring(this.filename.lastIndexOf('.') + 1);
    }


}
