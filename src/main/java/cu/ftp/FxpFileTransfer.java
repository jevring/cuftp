/**
 * Copyright (c) 2006, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */


package cu.ftp;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2006-jun-21
 */
public class FxpFileTransfer {
    private String sourceDir;
    private String sourceFilename;
    private String destinationDir;
    private String destinationFileName;
    private String type; // (I / A)
    private long size;

    public FxpFileTransfer(String sourceDir, String sourceFilename, String destinationDir, String destinationFileName, String type, long size) {
        // we're replaceing /// too, in case we have "blah/" + "/" + "/filename" 
        this.sourceDir = sourceDir.replaceAll("///", "/").replaceAll("//", "/");
        this.sourceFilename = sourceFilename;
        this.destinationDir = destinationDir.replaceAll("///", "/").replaceAll("//", "/");
        this.destinationFileName = destinationFileName;
        this.type = type;
        this.size = size;
    }

    public String getSourceDir() {
        return sourceDir;
    }

    public String getSourceFilename() {
        return sourceFilename;
    }

    public String getDestinationDir() {
        return destinationDir;
    }

    public String getDestinationFilename() {
        return destinationFileName;
    }

    public long getSize() {
        return size;
    }

    public String getType() {
        return type;
    }
}
