/**
 * Copyright (c) 2006, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftp;

import cu.ssl.DefaultSSLSocketFactory;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2006-jun-21
 */
public class FtpClient extends FtpConnection {
    protected ServerSocket activeDataConnection;
    protected boolean encryptedDataConnection;
    protected boolean usePassiveMode = true;
    protected int sndbufsize = 8192;
    protected int rcvbufsize = 8192;
    protected boolean skip0ByteFiles = false;
    protected boolean usePret = false;

    public FtpClient(Bnc bnc, String username, String password, String name) {
        this(bnc.getHostname(), bnc.getPort(), username, password, bnc.isEncryptControlConnection(), bnc.isEncryptDataConnection(), bnc.isUsePassiveMode(), bnc.getSslMode(), name);
    }

    public FtpClient(String hostname, int port, String username, String password, boolean encryptedControlConnection, boolean encryptedDataConnection, boolean usePassiveMode, int sslMode, String name) {
        super(hostname, port, username, password, encryptedControlConnection, sslMode, name);
        this.encryptedDataConnection = encryptedDataConnection;
        this.usePassiveMode = usePassiveMode;
    }

    /*
     * Only to be used if we're actually doing active mode up or download. For merely issuing commands, there are the port(String) and port(String, int) methods.
     */
    public void port() throws HostDisconnectedException {
        if (activeDataConnection != null) {
            port(activeDataConnection.getInetAddress().getHostAddress(), activeDataConnection.getLocalPort());
        } else {
            throw new IllegalStateException("Must create server socket first, use FtpClient.createActiveModeServerSocket()");
        }
    }

    public void port(String pasvString) throws HostDisconnectedException {
        InetSocketAddress isa = parsePassiveString(pasvString);
        port(isa.getAddress().getHostAddress(), isa.getPort());
    }
    public void port(String ip, int port) throws HostDisconnectedException {
        int highPort = port >> 8; // right-shift the value, to divide by 256 (except shifting is faster)
        int lowPort = port & 0xff; // AND the port by 255, effectively doing "% 255" to it
        String portString = "PORT " + ip.replace('.', ',') + "," + highPort + "," + lowPort;
        send(portString);
    }
    public InetSocketAddress parsePassiveString(String pasvString) {
        String portline = pasvString.substring(pasvString.indexOf("(") + 1, pasvString.lastIndexOf(")"));
        String[] data = portline.split(",");
        try {
            String ip = data[0] + "." + data[1] + "." + data[2] + "." + data[3];
            // shift is faster than multiply
            int highPort = Integer.parseInt(data[4]) << 8;
            int lowPort = Integer.parseInt(data[5]);
            int passivePort = highPort + lowPort;
            return new InetSocketAddress(ip, passivePort);

        } catch (ArrayIndexOutOfBoundsException e) {
            // this means we got an improper syntax, not enough parameters
        } catch (NumberFormatException e) {
            // and this means that we had stuff that are not digits in the string, beside the ",", which is also incorrect
        }
        throw new IllegalArgumentException(pasvString);

    }
    public InetSocketAddress pasv() throws FtpException {
        send("PASV");
        if (response.getCode() != 227) {
            throw new FtpException("Got erroneous command for PASV: " + response.getMessage(), name);
        }
        return parsePassiveString(response.getMessage());
        // also sets the passive string? or sets the passive ip and port maybe?
        // should maybe return an InternetAddress thing with ip and port
    }

    /**
     * This is PASV, but for encrypted site-to-site connections. Used instead of SSCN;[ON|OFF]
     * @return The InetSocketAddress object containing the IP and PORT that the socket is opened on
     * @throws FtpException thrown if we get any other response than 227
     */
    public InetSocketAddress cpsv() throws FtpException {
        send("CPSV");
        if (response.getCode() != 227) {
            throw new FtpException("Got erroneous command for CPSV: " + response.getMessage(), name);
        }
        return parsePassiveString(response.getMessage());
        // also sets the passive string? or sets the passive ip and port maybe?
        // should maybe return an InternetAddress thing with ip and port
    }

    // _TODO: implement the "NO timeout" stuff here for the commands that need it
    // NO, don't set that here, set it in the parse response, that knows what response codes require waiting.
    public void stor(String filename) throws HostDisconnectedException { send("STOR " + filename); }
    public void retr(String filename) throws HostDisconnectedException { send("RETR " + filename); }
    public void delete(String filename) throws HostDisconnectedException { send("DELE " + filename); }
    public void rmd(String dir) throws HostDisconnectedException {
        dir = dir.replaceAll("//", "/");
        send("RMD " + dir);
    }
    public void mkd(String dir) throws HostDisconnectedException {
        dir = dir.replaceAll("//", "/");
        send("MKD " + dir);
    }

    public boolean cdup() throws HostDisconnectedException {
        send("CDUP");
        if (response.getCode() == 250) {
            send("PWD");
            pwd = parsePwd(response.getMessage());
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<FtpFile> list() throws HostDisconnectedException {
        send("stat -l");
        String [] lines = response.getFullMessage().split("\n");
        ArrayList<FtpFile> result = new ArrayList<FtpFile>(Math.max(0, lines.length - 3)); // disregard the two first lines, and the last line

        for (int i = 0; i < lines.length; i++) {
            String line = lines[i];
            // apparently, stat -l on OpenBSD replies with the code 211
            if (line.startsWith("213") || line.startsWith("211") || line.startsWith("total")) {
                continue;
            }
            FtpFile ftpFile = new FtpFile(line, pwd);
            result.add(ftpFile);
        }
        return result;
    }

    public void recursiveDelete(String dir) throws CWDFailedException, HostDisconnectedException {
        boolean cwdOk = cwd(dir);
        if (cwdOk) {
            ArrayList files = list();
            Iterator i = files.iterator();
            while (i.hasNext()) {
                FtpFile ftpFile = (FtpFile) i.next();
                if (ftpFile.isDir()) {
                    // do this first. it's a bit more cumbersome, but that way we don't need to check if the dir is empty or not, which still requires entering it.
                    recursiveDelete(ftpFile.getAbsolutePath());
                    // NOTE: This might issue a couple of RMD too many, but at least it works
                    cdup(); // this so that we don't delete files in the wrong dir
                    rmd(dir);
                } else {
                    delete(ftpFile.getName());
                }
            }
            rmd(dir);
        } else {
            System.out.println("[" + name + "] Failed to cwd to " + dir + " for recursive delete");
            throw new CWDFailedException(dir, name);
        }
    }

    protected boolean prepareForDataTransfer(String remoteDir) throws IOException, FtpException {
        boolean cwdOk = true;
        if (!pwd.equals(remoteDir)) {
            cwdOk = cwd(remoteDir);
        }
        if (!cwdOk) {
            return false;
        }

        if (encryptedDataConnection) {
            send("PROT P");
        } // else PROT C?
        
        // check what mode we are using (PORT or PASV)
        // we default to passive mode, but it's changable
        if (usePassiveMode) {
            InetSocketAddress isa = pasv();
            if (encryptedDataConnection) {
                // note: can't send prot p here, it fucks things up
                dataConnection = DefaultSSLSocketFactory.getFactory().createSocket(isa.getHostName(), isa.getPort());
            } else {
                dataConnection = new Socket(isa.getHostName(), isa.getPort());
            }
        } else {
            if (encryptedDataConnection) {
                // todo: enable setting a port range for passive ports (do this by next int, not random. implement in cubnc also)
                activeDataConnection = DefaultSSLSocketFactory.getFactory().createServerSocket(0);
            } else {
                activeDataConnection = new ServerSocket(0);
            }
            port();
            dataConnection = activeDataConnection.accept();
        }
        dataConnection.setReceiveBufferSize(rcvbufsize);
        dataConnection.setSendBufferSize(sndbufsize);
        return true;
    }

    // add a comment here and below that skiplist can be null
    public void recursiveDownload(String localDir, String remoteDir, Skiplist skiplist) throws FtpException, IOException {
        if (!pwd.equals(remoteDir)) {
            // only cwd if we're not already there
            cwd(remoteDir);
        }
        if (pwd.equals(remoteDir)) {
            // we're in the dir, proceed do download
            ArrayList remoteFiles = list();
            Iterator i = remoteFiles.iterator();
            while (i.hasNext()) {
                FtpFile ftpFile = (FtpFile) i.next();
                if (ftpFile.isDir()) {
                    if (skiplist == null || !skiplist.skip(ftpFile.getName(), Skiplist.DIR)) {
                        recursiveDownload(localDir + "/" + ftpFile.getName(), remoteDir + "/" + ftpFile.getName(), skiplist);
                    }
                } else {
                    if (skiplist == null || !skiplist.skip(ftpFile.getName(), Skiplist.FILE)) {
                        download(localDir, remoteDir, ftpFile.getName(), ftpFile.getName());
                    }
                }
            }
        } else {
            throw new FtpException("[" + name + "] Unable to enter dir: " + remoteDir, name);
        }
    }

    public void recursiveUpload(String localDir, String remoteDir, Skiplist skiplist) throws FtpException, IOException {
        remoteDir = remoteDir.replaceAll("//", "/");
        localDir = localDir.replaceAll("//", "/");

        File local = new File(localDir);
        if (local.exists() && local.isDirectory()) {
            // only cwd if we're not already there
            boolean cwdOk = true;
            if (!pwd.equals(remoteDir)) {
                mkd(remoteDir);
                cwdOk = cwd(remoteDir);
            }
            if (cwdOk) {
                ArrayList remoteFiles = list();
                File [] files = local.listFiles();
                for (int i = 0; i < files.length; i++) {
                    FtpFile ftpFile = new FtpFile(files[i]);
                    if (ftpFile.isDir()) {
                        if (skiplist == null || !skiplist.skip(ftpFile.getName(), Skiplist.DIR)) {
                            recursiveUpload(ftpFile.getAbsolutePath(), remoteDir + "/" + ftpFile.getName(), skiplist);
                        }
                    } else {
                        if (!remoteFiles.contains(ftpFile)) {
                            // upload the ftpFile
                            if (skiplist == null || !skiplist.skip(ftpFile.getName(), Skiplist.FILE)) {
                                upload(files[i], remoteDir, ftpFile.getName());
                            }
                        } // else continue;
                    }
                }
            } else {
                throw new FtpException("[" + name + "] Unable to enter dir: " + remoteDir, name);
            }
        } else {
            throw new IllegalArgumentException("[" + name + "] Dir " + localDir + " either doesn't exist, or is not a directory.");
        }
    }

    /*
	 * Read about buffers here:
	 * http://groups.google.com/groups?hl=sv&lr=&ie=UTF-8&oe=UTF-8&threadm=9eomqe%24rtr%241%40to-gate.itd.utech.de&rnum=22&prev=/groups%3Fq%3Dtcp%2Bgood%2Bbuffer%2Bsize%26start%3D20%26hl%3Dsv%26lr%3D%26ie%3DUTF-8%26oe%3DUTF-8%26selm%3D9eomqe%2524rtr%25241%2540to-gate.itd.utech.de%26rnum%3D22
	 *
	 * Quote: Short answer is: if memory is not limited make your buffer big;
	 * TCP will flow control itself and only use what it needs.
	 *
	 * Longer answer: for optimal throughput (assuming TCP is not flow
	 * controlling itself for other reasons) you want your buffer size to at
	 * least be
	 *
	 * channel bandwidth * channel round-trip-delay.
	 *
	 * So on a long slow link, if you can get 100K bps throughput, but your
	 * delay -s 8 seconds, you want:
	 *
	 * 100Kbps * / bits-per-byte * 8 seconds = 100 Kbytes
	 *
	 * That way TCP can keep transmitting data for 8 seconds before it would
	 * have to stop and wait for an ack (to clear space in the buffer for new
	 * data so it can put new TX data in there and on the line). (The idea is to
	 * get the ack before you have to stop transmitting.)
	 */

    public boolean download(String localDir, String remoteDir, String localFilename, String remoteFilename) throws FtpException, IOException {
        String [] t = localFilename.split("\\.");
        return download(localDir, remoteDir, localFilename, remoteFilename, Filetypes.getFileType(t[t.length - 1]));
    }

    // add comment here and below about not using skiplist, since that should have been checked one level higher up
    public boolean download(String localDir, String remoteDir, String localFilename, String remoteFilename, String transferMode) throws IOException, FtpException {
        boolean prepared = prepareForDataTransfer(remoteDir);
        if (prepared) {
            int size = size(remoteFilename);
            if (skip0ByteFiles && size == 0) {
                return true;
            }
            if (usePret) {
                send("PRET RETR " + remoteFilename);
                if (response.getCode() == 500) {
                    return false;
                }
            }
            send("TYPE " + transferMode);
            retr(remoteFilename);
            if (response.getCode() == 150) {
                if ("A".equals(transferMode)) {
                    PrintWriter out = new PrintWriter(new FileWriter(new File(localDir, localFilename)));
                    BufferedReader br = new BufferedReader(new InputStreamReader(dataConnection.getInputStream()));
                    String line = "";
                    while ((line = br.readLine()) != null) {
                        out.println(line);
                    }
                    out.close();
                    br.close();
                } else {
                    FileOutputStream fos = new FileOutputStream(new File(localDir, localFilename));
                    BufferedInputStream in = new BufferedInputStream(dataConnection.getInputStream());
                    int c = -1;
                    byte [] buf = new byte [rcvbufsize];
                    long start = System.currentTimeMillis();
        //            int xx = 0;
                    while ((c = in.read(buf, 0, buf.length)) != -1) {
                        fos.write(buf, 0, c); // NOTE: this HAS to be "c", otherwise we'll write the entire buffer to file, instead of just the part of teh buffer that is filled
        //                xx += c;
                    }
                    double time = ((double)(System.currentTimeMillis() - start)) / 1000.0d;
                    System.out.println("time: " + time + " seconds");
                    System.out.println("size: " + fos.getChannel().size() + " bytes");
        //            System.out.println("size: " + xx + " bytes");
                    System.out.println("speed: " + (((double)fos.getChannel().size() / time) / 1024) + " kilobytes per second");
                    fos.close();
                    in.close();
                    dataConnection.close();
                }
                parseResponse(); // wait for the 226
                return true;
            } else {
                // we weren't allowed to download
                throw new DownloadFailedException(name, remoteFilename);
            }
        } else {
            throw new PreparationFailedException("download", name);
        }
    }

    public int size(String filename) throws HostDisconnectedException {
        send("SIZE " + filename);
        if (response.getCode() == 213) {
            String [] t = response.getMessage().split(" ");
            return Integer.parseInt(t[1]);
        } else {
            return -1;
        }
    }

    public boolean upload(File file, String remoteDir, String remoteFilename) throws IOException, FtpException {
        return upload(file, remoteDir, remoteFilename, Filetypes.getFileType(file.getName().substring(file.getName().lastIndexOf('.') + 1)));
    }

    public boolean upload(File file, String remoteDir, String remoteFilename, String transferMode) throws IOException, FtpException {
        boolean prepared = prepareForDataTransfer(remoteDir);
        if (prepared) {
            if (skip0ByteFiles && file.length() == 0) {
                return true;
            }
            if (usePret) {
                send("PRET STOR " + remoteFilename);
                if (response.getCode() == 500) {
                    return false;
                }
            }
            send("TYPE " + transferMode);
            stor(remoteFilename);
            if (response.getCode() == 150) {
                if ("A".equals(transferMode)) {
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    PrintWriter out = new PrintWriter(dataConnection.getOutputStream());
                    String line = "";
                    while ((line = br.readLine()) != null) {
                        out.println(line);
                    }
                    out.close();
                    br.close();
                    dataConnection.close();
                } else {
                    FileInputStream in = new FileInputStream(file);
                    BufferedOutputStream bos = new BufferedOutputStream(dataConnection.getOutputStream());
                    int c = -1;
                    byte [] buf = new byte [sndbufsize];
                    long start = System.currentTimeMillis();
                    while ((c = in.read(buf, 0, buf.length)) != -1) {
                        bos.write(buf, 0, c);
                    }
                    double time = ((double)(System.currentTimeMillis() - start)) / 1000.0d;
                    System.out.println("time: " + time + " seconds");
                    System.out.println("size: " + in.getChannel().size() + " bytes");
                    System.out.println("speed: " + (((double)in.getChannel().size() / time) / 1024) + " kilobytes per second");
                    // _todo: how do we get this data out to the caller without unnecessarily high coupling? (we don't, we fuck it)
                    bos.close();
                    in.close();
                    dataConnection.close();
                }
                parseResponse(); // wait for the 226
                return true;
            } else {
                // we weren't allowed to upload!
                throw new UploadFailedException(name, remoteFilename);
            }
        } else {
            throw new PreparationFailedException("upload", name);
        }
    }

    public boolean usePassiveMode() {
        return usePassiveMode;
    }
    public void setUsePassiveMode(boolean usePassiveMode) {
        this.usePassiveMode = usePassiveMode;
    }

    public boolean isTransferring() {
        return transferring;
    }

    public void setSkip0ByteFiles(boolean skip0ByteFiles) {
        this.skip0ByteFiles = skip0ByteFiles;
    }

    public String getName() {
        return name;
    }

    public boolean isUsePret() {
        return usePret;
    }

    public void setUsePret(boolean usePret) {
        this.usePret = usePret;
    }

    public boolean isEncryptedDataConnection() {
        return encryptedDataConnection;
    }

    public int getSndbufsize() {
        return sndbufsize;
    }

    public void setSndbufsize(int sndbufsize) {
        this.sndbufsize = sndbufsize;
    }

    public int getRcvbufsize() {
        return rcvbufsize;
    }

    public void setRcvbufsize(int rcvbufsize) {
        this.rcvbufsize = rcvbufsize;
    }

    public String getUsername() {
        return username;
    }
}
