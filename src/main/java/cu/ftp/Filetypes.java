package cu.ftp;

import java.util.HashMap;

/**
 * Copyright (c) 2006, Markus Jevring <markus@jevring.net>
 * <p/>
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 * <p/>
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
public class Filetypes {
    private static HashMap<String, String> types = new HashMap<String, String>();
    private static String defaultFileType = "I";

    static {
        types.put("gz", "I");
        types.put("rar", "I");
        types.put("zip", "I");
        types.put("mp3", "I");
        types.put("r**", "I");
        types.put("gz", "I");
        types.put("tar", "I");
        types.put("txt", "A");
        types.put("nfo", "I"); // we ran into size difference problems when we transfered this as "A"
        types.put("diz", "A");

    }

    public static void load(String filename) {

    }

    public static void load(HashMap<String, String> types) {
        Filetypes.types = types;
    }

    public static void setDefaultFileType(String defaultFileType) {
        Filetypes.defaultFileType = defaultFileType;
    }

    public static String getFileType(String extention) {
        String type = types.get(extention);
        if (type == null) { type = defaultFileType; }
        return type;
    }
}
