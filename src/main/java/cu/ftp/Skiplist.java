/**
 * Copyright (c) 2006, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */


package cu.ftp;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2006-okt-08
 */
public class Skiplist {
    private ArrayList<Pattern> dirPatterns;
    private ArrayList<Pattern> filePatterns;
    public static final int FILE = 0;
    public static final int DIR = 1;


    public Skiplist() {
        dirPatterns = new ArrayList<Pattern>();
        filePatterns = new ArrayList<Pattern>();
    }

    public void addPattern(String pattern, int type, boolean convertToRegexp) {
        if (convertToRegexp) {
            pattern = pattern.replace("*", "(.*)").replace("?", "(.?)");
        }
        Pattern p = Pattern.compile(pattern);
        if (type == Skiplist.FILE) {
            filePatterns.add(p);
        } else if (type == Skiplist.DIR) {
            dirPatterns.add(p);
        } else {
            throw new IllegalArgumentException("Can't add patterns of that type");
        }

    }

    public boolean skip(String name, int type) {
        ArrayList<Pattern> l;
        if (type == Skiplist.FILE) {
            l = filePatterns;
        } else if (type == Skiplist.DIR) {
            l = dirPatterns;
        } else {
            throw new IllegalArgumentException("Can't check skipping for that type");
        }
        for (Pattern p : l) {
            if (p.matcher(name).matches()) {
                return true;
            }
        }
        return false;
  }
}
